import { Component, OnInit } from '@angular/core';
import { TestService } from './test.service';
import { Test } from './test';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(private testService: TestService) { }


  ngOnInit() {
    this.showTest();
  }

  showTest() {
    this.testService.postTest()
      .subscribe((data: any) => console.log(data));
  }
}