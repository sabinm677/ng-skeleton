import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { SearchComponent } from './search/search.component';
import { RouterModule } from '@angular/router';
import { InterceptorModule } from './interceptor.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    InterceptorModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: SearchComponent },
      { path: 'search', component: SearchComponent },
      { path: 'hello', component: TestComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
