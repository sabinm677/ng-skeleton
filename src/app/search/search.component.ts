import { Component, OnInit, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  showSuggestionForm: boolean;
  d: Array<Object> = [];
  arr = [{ id:1, name: 'first'}, 
    {id:2,  name: 'second'}, 
    {id:3,  name: 'third'}
  ];
  nodes = [{id:1, name: 'root', children: [
    {id:2, name: 'a', 'node_id':1, children: []},
    {id:3, name: 'b','node_id':1, children: []},
    {id:4, name: 'c','node_id':1, children: [
        {id:5, name: 'd','node_id':4, children: []},
        {id:6, name: 'e','node_id':4, children: []},
        {id:7, name: 'f','node_id':4, children: [
            {id:8, name: 'g','node_id':7, children: []},
          ]},
      ]},
  ]},
  {id:1, name: 'root', children: [
    {id:2, name: 'a', 'node_id':1, children: []},
    {id:3, name: 'b','node_id':1, children: []},
    {id:4, name: 'c','node_id':1, children: [
        {id:5, name: 'd','node_id':4, children: []},
        {id:6, name: 'e','node_id':4, children: []},
        {id:7, name: 'f','node_id':4, children: [
            {id:8, name: 'g','node_id':7, children: []},
          ]},
      ]},
  ]}
];
  constructor() { }

  ngOnInit() {
  }

  showSuggestions(e) {
    let val = e.target.value;
    if(val.length > 2) {
      this.showSuggestionForm = true;
    } else {
      this.showSuggestionForm = false;
    }

    // console.log(this.showSuggestionForm);
  }

  selectSuggestion(id) {
    let x = this.arr.find( y => {
      return (y.id == id);
    });
    // console.log(x);
    this.d.push(x);
  }

  togglePrimary(x) {
    if('primary' in x && x.primary == true) {
      x.primary = false;
    } else {
      x.primary = true;
    }

  }

}
