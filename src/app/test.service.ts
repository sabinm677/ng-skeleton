import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Test } from './test';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }

  public url = 'http://localhost:8000/api/test';

  getTest() {
    return this.http.get<Test>(this.url);
  }

  postTest() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': 'http://localhost:8000',
        'Content-Type':  'application/json',
      })
    };
    return this.http.post(this.url, {});
  }

}
