import { Component, OnInit } from '@angular/core';
import { TestService } from '../test.service';
import { Test } from '../test';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  private testService: TestService;

  constructor() { }

  ngOnInit() {
    this.showTest();
  }

  showTest() {
    this.testService.getTest()
      .subscribe((data: Test) => console.log(data));
  }

}
